from .models import Contenido
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from django.contrib.auth import logout
from django.shortcuts import redirect

# Create your views here.

form = """
<form action="" method="POST">
    <p>Introduce el valor: <input type="text" name="valor"/>
    <p><input type="submit" value="Enviar"/>
</form>
"""

@csrf_exempt
def get_content(request, llave):
    if request.method == "PUT":
        valor = request.body.decode('utf-8')
        try:
            c = Contenido.objects.get(clave=llave)
            c.valor = valor
        except Contenido.DoesNotExist:
            c = Contenido(clave=llave, valor=valor)
        c.save()

    if request.method == "POST":
        valor = request.POST['valor']
        try:
            c = Contenido.objects.get(clave=llave)
            c.valor = valor
        except Contenido.DoesNotExist:
            c = Contenido(clave=llave, valor=valor)
        c.save()
    try:
        contenido = Contenido.objects.get(clave=llave)
        respuesta = "El valor de la clave " + contenido.clave \
                    + " es " + contenido.valor + ", y su id es " + str(contenido.id)
    except Contenido.DoesNotExist:
        if request.user.is_authenticated:
            respuesta = "Eres " + request.user.username + "<br>"
            respuesta += "No existe contenido para " + llave + form
        else:
            respuesta = "You must log in to add new content. <a href='/login'>Log in here</a>"
    return HttpResponse(respuesta)

def index(request):
    content_list = Contenido.objects.all()
    template = loader.get_template('cms_users_put/index.html')
    context = {
        'content_list': content_list,
    }
    return HttpResponse(template.render(context, request))

def loggedIn(request):
    if request.user.is_authenticated:
        respuesta = "Logged in as " + request.user.username
    else:
        respuesta = "Not logged in. <a href='/admin'>Login via admin</a>"
    return HttpResponse(respuesta)

def logout_view(request):
    logout(request)
    return redirect('/cms_put')
